package nl.kza.swat.tests;

import io.qameta.allure.Attachment;
import io.qameta.allure.Issue;
import io.qameta.allure.Step;
import nl.kza.swat.models.*;
import nl.kza.swat.pages.StartPaginaVoorLogin;
import nl.kza.swat.ui.SharedDriver;
import nl.kza.swat.util.YamlLoader;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static nl.kza.swat.ui.SharedDriver.setOriginalHandle;

public class Tester {

    private static WebDriver driver;
    //private static final String StartURL = "http://www.kza.nl";
    private final Logger LOG = LoggerFactory.getLogger(Tester.class);
    private static StartPaginaVoorLogin startPagina;
    private static User user;
    private static Customer customer;
    private static AdminUser adminUser;
    private static YamlLoader yamlLoader = new YamlLoader();

    @BeforeClass
    public static void prepareTestSuite(){
        //Add log entry to mark the start of this test
        LoggerFactory.getLogger(Tester.class).info("Starting TestClass Tester");

        //Get a webdriver instance and set an entry for the main BrowserTab.
        driver = new SharedDriver().getBrowserInstance();
        setOriginalHandle(driver.getWindowHandle());

        //Instantiate the first page of the testobject
        startPagina = new StartPaginaVoorLogin(driver);

        //load config from yaml files
        user = yamlLoader.CreateUserFromYaml("testgebruiker");
        customer = yamlLoader.CreateCustomerFromYaml("testklant");
        adminUser = yamlLoader.CreateAdminUserFromYaml("admingebruiker");
    }

    @Before
    public void prepareTestCase(){
        //before each test, return to the main page. The cookies are deleted, so for each test we need to login.
        startPagina.openUrl();
    }

    @Step
    public void testUrlIsNotNull(){
        Assert.assertTrue(startPagina!=null);
    }

    @Test
    @Issue("LoginCase")
    public void AanmeldenCollega(){
        startPagina.GoToAanmeldPagina().DoMaakAccountAan(
                    user.getName()
                    , user.getGender()
                    , user.getPlaceOfBirth()
                    , user.getBirthDate()
                    , user.getEmail()
                    , user.getPhoneNumber()
                    , user.getInServiceDate()
                    , user.getPassword())
                .assertTitleIs("Gebruikersprofiel");
    }

    @Test
    public void InloggenCollega() {
        startPagina.GoToLoginPagina()
                .DoLogIn(user.getEmail(), user.getPassword())
                .assertTitleIs("Gebruikersprofiel");
    }

    @Test
    public void OpvragenCollegaDetailPagina(){
        startPagina.GoToLoginPagina()
                .DoLogIn(user.getEmail(), user.getPassword())
                .GoToCollegaOverzicht()
                .GoToCollegaDetailPagina(user.getName())
                .WithNaam(user.getName())
                .WithEmail(user.getEmail())
                .WithGroep(user.getGroup()[0])
                .WithKennisgebied(user.getKnowledgeArea()[0])
                .WithErvaring(user.getExperience()[0])
                .WithCursus(user.getCourses()[0]);
    }

    @Test
    public void OpvragenKlantDetailPagina(){
        startPagina.GoToLoginPagina()
                .DoLogIn(user.getEmail(), user.getPassword())
                .GoToKlantOverzicht()
                .GoToKlantDetailPagina(customer.getName())
                .WithNaam(customer.getName())
                .WithBeschrijving(customer.getDescription())
                .WithKennisgebied(customer.getKnowledgeArea()[0])
                .WithKZAers(customer.getSecondedKZA()[0]);
    }

    @Test
    public void PlaatsenBericht(){
        startPagina.GoToLoginPagina()
                .DoLogIn(user.getEmail(), user.getPassword())
                .GoToHome()
                .DoTypBericht(user.getMessage())
                .DoPlaatsBericht()
                .WithBericht(user.getName(), user.getMessage())
                .VerwijderBericht(user.getName(), user.getMessage());
    }


    @After
    public void finishTestCase(){
        //Ugly wait in thread to have some time to see the result of the test. Only for debugging purposes.
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
           LOG.error("Thread interrupted",e);
        }

        //Delete cookies from the browser. The browser stay's 'open' between tests. This is to improve the performance
        //As each time the brower is restarted it adds +- 4-6 seconds to the test duration.
        //To let the test inherit the browser session of the previous test (!! NOT a good practice !!) disable the delete cookies statement.
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public static void finishTestSuite(){

    }
}