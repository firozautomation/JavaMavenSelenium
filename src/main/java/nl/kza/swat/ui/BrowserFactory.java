package nl.kza.swat.ui;

import nl.kza.swat.util.TestConfig;
import org.openqa.selenium.WebDriver;

public class BrowserFactory {

    public static WebDriver getBrowser() {
        String desiredBrowserName = System.getProperty("browser", TestConfig.valueFor("UseBrowser"));
        WebDriver desiredBrowser = null;

        try {
            switch (desiredBrowserName) {
                case "ie":
                    desiredBrowser = IEBrowser.buildIEBrowser();
                    break;
                case "chrome":
                    desiredBrowser = ChromeBrowser.buildChromeBrowser();
                    break;
                case "firefox":
                    desiredBrowser = FirefoxBrowser.buildFirefoxBrowser();
                    break;
                case "remote":
                    desiredBrowser = new RemoteBrowser().getDriver();
                default:
                    //TODO:work out what to do when a browser isn't needed
                    break;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return desiredBrowser;
    }
}
