package nl.kza.swat.basepages;

import nl.kza.swat.pages.CollegaOverzichtPagina;
import nl.kza.swat.pages.KlantOverzichtPagina;
import nl.kza.swat.pages.StartPaginaNaLogin;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static nl.kza.swat.models.Messages.errorMessages;

public class HeaderFooter extends Base{

	 @FindBy(id = "start") 		
	 public WebElement lnkStart;
	
	 @FindBy(id = "home") 		
	 public WebElement lnkKZACONNECTED;
	 
	 @FindBy(css = "ul.dropdown-menu > li > a") 		
	 public List<WebElement> listAlleNavigeerOpties;
	
	public HeaderFooter(WebDriver webDriver) {
		super(webDriver);
	}
	
	public StartPaginaNaLogin GoToHome()
    {
        clickOnElement(lnkKZACONNECTED);
        return new StartPaginaNaLogin(webDriver);
    }

    public CollegaOverzichtPagina GoToCollegaOverzicht()
    {
        clickOnElement(lnkStart);
        clickMenuItem("Collega's");
        return new CollegaOverzichtPagina(webDriver);
    }

    public KlantOverzichtPagina GoToKlantOverzicht()
    {
        clickOnElement(lnkStart);
        clickMenuItem("Klanten");
        return new KlantOverzichtPagina(webDriver);
    }

    private void clickMenuItem(String menuitem)
    {
        for (WebElement navigeeroptie : listAlleNavigeerOpties)
        {
            if (navigeeroptie.getText().equals(menuitem))
            {
                clickOnElement(navigeeroptie);
                return;
            }
        }
        Assert.fail(errorMessages.get("MenuItemNietGevonden"));
        return;
    }
	
}
