package nl.kza.swat.models;

/**
 * Created by Wluijk on 7/19/2017.
 */
public class Customer {
    private String name;
    private String description;
    private String customerSince;
    private String[] knowledgeArea;
    private String[] secondedKZA;

    public Customer(String name, String description, String customerSince, String[] knowledgeArea, String[] secondedKZA) {
        this.name = name;
        this.description = description;
        this.customerSince = customerSince;
        this.knowledgeArea = knowledgeArea;
        this.secondedKZA = secondedKZA;
    }

    public Customer(){ //default values when constructor is empty
        this("NS"
                ,"TjoekeTjoeke"
                ,"10102010"
                ,new String[]{"Treinen"}
                ,new String[]{"Jan"});
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomerSince() {
        return customerSince;
    }

    public void setCustomerSince(String customerSince) {
        this.customerSince = customerSince;
    }

    public String[] getSecondedKZA() {
        return secondedKZA;
    }

    public void setSecondedKZA(String[] secondedKZA) {
        this.secondedKZA = secondedKZA;
    }

    public String[] getKnowledgeArea() {
        return knowledgeArea;
    }

    public void setKnowledgeArea(String[] knowledgeArea) {
        this.knowledgeArea = knowledgeArea;
    }
}
