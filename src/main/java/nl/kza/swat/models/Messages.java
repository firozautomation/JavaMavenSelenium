package nl.kza.swat.models;

import java.util.HashMap;

public class Messages {

    public static final HashMap<String,String> errorMessages = new HashMap<>();

    public Messages(){
        constructErrorMessages();
    }

    private static void constructErrorMessages(){
        errorMessages.put("Geslacht","Onbekend geslacht in Testgebruiker");
        errorMessages.put("GebruikerTitel", "Titel komt niet overeen, gevonden titel: ");
        errorMessages.put("GebruikerNaam","Naam komt niet overeen, gevonden naam");
        errorMessages.put("GebruikerEmail","Email komt niet overeen, gevonden email: ");
        errorMessages.put("GebruikerKennisgebied","Kennisgebied komt niet voor in lijst.");
        errorMessages.put("GebruikerGroep","Groep komt niet voor in lijst.");
        errorMessages.put("GebruikerKlant","Klant komt niet voor in lijst.");
        errorMessages.put("GebruikerCursus","Cursus komt niet voor in lijst.");
        errorMessages.put("GebruikerNietGevonden","Gebruiker niet gevonden in het Collegaoverzicht.");
        errorMessages.put("KlantTitel","Titel komt niet overeen, gevonden titel: ");
        errorMessages.put("KlantNaam","Naam komt niet overeen, gevonden naam: ");
        errorMessages.put("KlantBeschrijving","Email komt niet overeen, gevonden email: ");
        errorMessages.put("KlantKennisgebied","Kennisgebied komt niet voor in lijst.");
        errorMessages.put("KlantGebruiker","Groep komt niet voor in lijst.");
        errorMessages.put("KlantNietGevonden","Klant niet gevonden in het klantenoverzicht.");
        errorMessages.put("MenuItemNietGevonden","MenuItem niet gevonden.");
        errorMessages.put("BerichtNietGevonden","Bericht niet gevonden.");
    }
}
