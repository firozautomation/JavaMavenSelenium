package nl.kza.swat.models;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Wluijk on 7/18/2017.
 */
public class User {
    private String name;
    private String gender;
    private String placeOfBirth;
    private String birthDate;
    private String phoneNumber;
    private String email;
    private String inServiceDate;
    private String[] group;
    private String[] knowledgeArea;
    private String[] experience;
    private String[] courses;
    private String password;
    private String message;

    public User(String name, String gender, String placeOfBirth, String birthDate, String phoneNumber, String email, String inServiceDate, String[] group, String[] knowledgearea, String[] experience, String[] courses, String password, String message) {
        this.name = name;
        this.gender = gender;
        this.placeOfBirth = placeOfBirth;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.inServiceDate = inServiceDate;
        this.group = group;
        this.knowledgeArea = knowledgeArea;
        this.experience = experience;
        this.courses = courses;
        this.password = password;
        this.message = message;
    }

    public User(){
        this("Henk"
                ,"M"
                ,"Rome"
                ,"05101800"
                ,"0618521648"
                ,"jan@test.nl"
                ,"10102016"
                , new String[]{"TA Guild"}
                , new String[]{"Java"}
                , new String[]{"Telfort"}
                , new String[]{"SQL"},"123123","Message");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInServiceDate() {
        return inServiceDate;
    }

    public void setInServiceDate(String inServiceDate) {
        this.inServiceDate = inServiceDate;
    }

    public String[] getGroup() {
        return group;
    }

    public void setGroup(String[] group) {
        this.group = group;
    }

    public String[] getKnowledgeArea() {
        return knowledgeArea;
    }

    public void setKnowledgeArea(String[] knowledgearea) {
        this.knowledgeArea = knowledgearea;
    }

    public String[] getExperience() {
        return experience;
    }

    public void setExperience(String[] experience) {
        this.experience = experience;
    }

    public String[] getCourses() {
        return courses;
    }

    public void setCourses(String[] courses) {
        this.courses = courses;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}