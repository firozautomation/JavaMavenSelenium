package nl.kza.swat.pages;


import nl.kza.swat.basepages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePagina extends Base {

	@FindBy(xpath = "//*[@id='menu-item-75']/a")
	private WebElement contactButton;

	public HomePagina(WebDriver webDriver) {
		super(webDriver);
	}
	
	public ContactPagina ClickContact()
	{
		clickOnElement(contactButton);
		return new ContactPagina(webDriver);
	}
}
