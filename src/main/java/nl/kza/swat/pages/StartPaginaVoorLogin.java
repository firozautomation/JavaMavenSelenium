package nl.kza.swat.pages;

import nl.kza.swat.basepages.Base;
import nl.kza.swat.util.TestConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StartPaginaVoorLogin extends Base {

    private static final String STARTURL = TestConfig.valueFor("StartURL");
		
    @FindBy(id = "login")
    public WebElement lnkLogIn;

    @FindBy(id = "signup")
    public WebElement lnkMeldJeNuAan;
    	
	public StartPaginaVoorLogin(WebDriver webDriver) {
		super(webDriver);
	}

    public InlogPagina GoToLoginPagina()
    {
        clickOnElement(lnkLogIn);
        return new InlogPagina(webDriver);
    }

    public AanmeldPagina GoToAanmeldPagina()   
    {
        clickOnElement(lnkMeldJeNuAan);
        return new AanmeldPagina(webDriver);
    }

    public StartPaginaVoorLogin openUrl(){
	    webDriver.navigate().to(STARTURL);
        return this;
    }
}
