package nl.kza.swat.pages;

import nl.kza.swat.basepages.Base;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static nl.kza.swat.models.Messages.errorMessages;

public class KlantDetailPagina extends Base {
	
	
	@FindBy(tagName = "h1")
    private WebElement lblTitel;   
	
	@FindBy(css = "label.static-text[for='name']")
    private WebElement lblNaam;   
 
	@FindBy(css = "label.static-text[for='description']")
    private WebElement lblBeschrijving;   

    @FindBy(css = "span.label-primary")
    private List<WebElement> listAlleBlauweLabeltjes;   
    	    
    @FindBy(css = "table.table-condensed > tbody > tr > td")
    private List<WebElement> listAlleKZAersCellen;

    public KlantDetailPagina(WebDriver webDriver) {
		super(webDriver);
	}
	/*
	 * 
	 */
	public KlantDetailPagina WithTitel(String titel)
    {
        Assert.assertEquals(titel, getText(lblTitel), errorMessages.get("KlantTitel") + getText(lblTitel));
        return this;
    }

    public KlantDetailPagina WithNaam(String naam)
    {
        Assert.assertEquals(naam, getText(lblNaam), errorMessages.get("KlantNaam")  +  getText(lblNaam));
        return this;
    }

    public KlantDetailPagina WithBeschrijving(String email)
    {
        Assert.assertEquals(email, getText(lblBeschrijving), errorMessages.get("KlantBeschrijving") +  getText(lblBeschrijving));
        return this;
    }

    public KlantDetailPagina WithKennisgebied(String kennisgebied)
    {
        for (WebElement blauwLabeltje : listAlleBlauweLabeltjes)
        {
            if (getText(blauwLabeltje).contains(kennisgebied))
            {
                WebElement blok = blauwLabeltje
                     .findElement(By.xpath(".."))                           //parent
                     .findElement(By.xpath(".."))                           //parent
                     .findElement(By.xpath("preceding-sibling::*[1]"))      //sibling
                     .findElement(By.tagName("small"));
                if (getText(blok).contains("Kennisgebieden"))
                    return this;
            }
        }
        Assert.fail(errorMessages.get("KlantKennisgebied"));
        return this;
    }

    public KlantDetailPagina WithKZAers(String kZAer)
    {
        for (WebElement cel : listAlleKZAersCellen)
        {
            if (getText(cel).equals(kZAer))
            {
                return this;
            }
        }
        Assert.fail(errorMessages.get("GebruikerKlant"));
        return this;
    }
}