package nl.kza.swat.pages;


import nl.kza.swat.basepages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ContactPagina extends Base {

	@FindBy(xpath = "//*[@id='menu-item-83']/a")
	private WebElement homeButton;

	public ContactPagina(WebDriver webDriver) {
		super(webDriver);
	}
	
	public HomePagina ClickHome()
	{
		clickOnElement(homeButton);
		return new HomePagina(webDriver);
	}
}

