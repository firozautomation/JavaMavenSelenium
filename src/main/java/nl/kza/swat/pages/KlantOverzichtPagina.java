package nl.kza.swat.pages;


import nl.kza.swat.basepages.Base;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static nl.kza.swat.models.Messages.errorMessages;

public class KlantOverzichtPagina extends Base {
		
	@FindBy(css = "ul.clients > li > a")
    private List<WebElement> listAlleKlanten;
	
	public KlantOverzichtPagina(WebDriver webDriver) {
		super(webDriver);
	}

	public KlantDetailPagina GoToKlantDetailPagina(String klant)
    {
        //First check if we have a valid list. Currently the test will just fail if there are no search results
        //For the search action the customer name from the yaml file is used.
        if(listAlleKlanten.size()<1){
            Assert.fail("No customers are found for this search action");
            return null;
        }

        for (WebElement element : listAlleKlanten)
        {
            if (textFromElementIsEqual(element,klant))
            {
                clickOnElement(element);
                return new KlantDetailPagina(webDriver);
            }
        }
        Assert.fail(errorMessages.get("KlantNietGevonden"));
        return new KlantDetailPagina(webDriver);
    }
}
